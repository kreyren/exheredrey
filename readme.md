# Exheredrey
This is personal repository of Jacob Hrbek for personal use.

!!! NOT PART OF EXHERBO !!!

![](https://media.giphy.com/media/WMyHsTLLKlBsc/giphy.gif)

### Installation (WIP)
Parse following in /etc/paludis/repositories/exheredrey.conf
```conf
format = e
location = /var/db/paludis/repositories/exheredrey
sync = git+ssh://git@gitlab.exherbo.org/kreyren/exheredrey.git
```

TODO: Push in unavailable so that it can be grabbed using `cave resolve -x1 repository/exheredrey` once it's worth sharing

### Code Of Conduct (WIP)
1) DevOps everything -> Make everything automatic
